package houston

import (
	"encoding/json"
	"reflect"
)

func ValidateSession(request *Passenger, elem reflect.Value) (*ErrorBody) {
	if request.Headers == nil || request.Headers["session"] == nil {
		return &ErrorBody{Message: DEFAULT_ERROR_MSG, Code: HTTP_BAD_REQUEST, InternalMessage: "Missing required session header attribute"}
	}
	var session Session
	json.Unmarshal([]byte(string(request.Headers["session"].(string))), &session)
	if (session.SelectedAccountUserId == 0) {
		return &ErrorBody{Message: DEFAULT_ERROR_MSG, Code: HTTP_BAD_REQUEST, InternalMessage: "Missing required header attribute session.selectedAccountUserId"}
	}
	if (session.SelectedAccountId == 0) {
		return &ErrorBody{Message: DEFAULT_ERROR_MSG, Code: HTTP_BAD_REQUEST, InternalMessage: "Missing required header attribute session.selectedAccountId"}
	}
	if (session.SelectedEnvironmentId == "") {
		return &ErrorBody{Message: DEFAULT_ERROR_MSG, Code: HTTP_BAD_REQUEST, InternalMessage: "Missing required header attribute session.selectedEnvironmentId"}
	}
	if (session.SelectedProviderEnvironmentId == "") {
		return &ErrorBody{Message: DEFAULT_ERROR_MSG, Code: HTTP_BAD_REQUEST, InternalMessage: "Missing required header attribute session.selectedProviderEnvironmentId"}
	}

	sessionField := elem.FieldByName("Session")
	if sessionField.IsValid() {
		sessionValue := reflect.ValueOf(session)
		sessionField.Set(sessionValue)
	}

	environmentIdField := elem.FieldByName("EnvironmentId")
	if environmentIdField.IsValid() {
		environmentIdField.SetString(session.SelectedEnvironmentId)
	}

	providerEnvironmentIdField := elem.FieldByName("ProviderEnvironmentId")
	if providerEnvironmentIdField.IsValid() {
		providerEnvironmentIdField.SetString(session.SelectedProviderEnvironmentId)
	}
	return nil
}
