package houston

import "reflect"
import "time"

const (
	POST   = "POST"
	GET    = "GET"
	PUT    = "PUT"
	DELETE = "DELETE"
)

const (
	DEFAULT_ERROR_MSG = "Sorry, something went wrong. Please try again later."
)

const (
	HTTP_OK                     = 200
	HTTP_CREATED                = 201
	HTTP_ACCEPTED               = 202
	HTTP_NO_CONTENT             = 204
	HTTP_BAD_REQUEST            = 400
	HTTP_UNAUTHORIZED           = 401
	HTTP_FORBIDDEN              = 403
	HTTP_NOT_FOUND              = 404
	HTTP_METHOD_NOT_ALLOWED     = 405
	HTTP_NOT_ACCEPTABLE         = 406
	HTTP_CONFLICT               = 409
	HTTP_UNSUPPORTED_MEDIA_TYPE = 415
	HTTP_INTERNAL_SERVER_ERROR  = 500
	HTTP_SERVICE_UNAVAILABLE    = 503
	HTTP_GATEWAY_TIMEOUT        = 504
)

var emptyMsgType = reflect.TypeOf(&Passenger{})

type Param struct {
	Name  string
	Index int
}
type Params []Param

type Middleware func(*Passenger, reflect.Value) *ErrorBody
type Middlewares []Middleware

type Rockets []Rocket
type Rocket struct {
	Route      string
	Params     Params
	Group      string
	Middleware Middlewares
	Callback   interface{}
}

type Headers map[string]interface{}
type Passenger struct {
	Subject         string `json:"subject,omitempty"`
	Reply           string `json:"reply,omitempty"`
	StatusCode      int `json:"statusCode,omitempty"`
	Headers         Headers `json:"headers,omitempty"`
	Body            string `json:"body,omitempty"`
	TimeoutOverride time.Duration `json:"timeoutOverride,omitempty"`
}

type Session struct {
	SelectedAccountUserId         int `json:"selectedAccountUserId,omitempty" sql:"-"`
	SelectedAccountId             int `json:"selectedAccountId,omitempty" sql:"-"`
	SelectedEnvironmentId         string `json:"selectedEnvironmentId,omitempty" sql:"-"`
	SelectedProviderEnvironmentId string `json:"selectedProviderEnvironmentId,omitempty" sql:"-"`
}

type ErrorResponse struct {
	Err ErrorBody    `json:"err,omitempty"`
}

type ErrorBody struct {
	Code            int    `json:"code,omitempty"`
	Message         string `json:"msg,omitempty"`
	InternalMessage string    `json:"internalMessage,omitempty"`
}

func (e *ErrorBody) Error() string {
	return e.InternalMessage
}
