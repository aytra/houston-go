package houston

import (
	"github.com/nats-io/nats"
	"encoding/json"
	"reflect"
	"strings"
	"errors"
	log "github.com/Sirupsen/logrus"
	"time"
)

type ControlTower struct {
	NatsEncodedConn    *nats.EncodedConn
	NatsConn           *nats.Conn
	NatsUrl            string
	NatsNextMsgTimeout time.Duration
}

func (controlTower *ControlTower) Init() {
	natsConnection, natsErr := nats.Connect(controlTower.NatsUrl,
		nats.ErrorHandler(func(nc *nats.Conn, sub *nats.Subscription, err error) {
			log.WithFields(log.Fields{
				"nats": "true",
			}).Errorf("Nats ErrorHandler %s - %s\n", err, sub)
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			log.WithFields(log.Fields{
				"nats": "true",
			}).Warnf("NATS connection closed to %v+ - %v+ - %v+ - %v+\n", nc.ConnectedUrl(), nc.LastError(), nc.Stats(), nc.Status())
		}),
		nats.DisconnectHandler(func(nc *nats.Conn) {
			log.WithFields(log.Fields{
				"nats": "true",
			}).Errorf("NATS got disconnected %v+ - %v+ - %v+ - %v+\n", nc.ConnectedUrl(), nc.LastError(), nc.Stats(), nc.Status())
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			log.WithFields(log.Fields{
				"nats": "true",
			}).Warnf("NATS got reconnected to %v+ - %v+ - %v+ - %v+\n", nc.ConnectedUrl(), nc.LastError(), nc.Stats(), nc.Status())
		}))

	if natsErr != nil {
		log.Panicf("Error connecting to NATS server %s", natsErr.Error())
	}

	if controlTower.NatsNextMsgTimeout == 0 {
		controlTower.NatsNextMsgTimeout = 10 * time.Second
	}

	natsEncodedConnection, _ := nats.NewEncodedConn(natsConnection, nats.JSON_ENCODER)

	controlTower.NatsConn = natsConnection
	controlTower.NatsEncodedConn = natsEncodedConnection
}

func (controlTower *ControlTower) Build(rocket Rocket) (func(p *Passenger), error) {
	return controlTower.QueueSubscribe(rocket.Route, rocket.Group, rocket.Callback, rocket.Params, rocket.Middleware)
}

func (controlTower *ControlTower) QueueSubscribe(subject string, queue string, cb interface{}, params Params, middlewares Middlewares) (func(p *Passenger), error) {
	log.Debugf("Creating rocket for %s", subject)
	argType, numArgs := argInfo(cb)
	if argType == nil {
		return nil, errors.New("Handler requires at least one argument")
	}
	cbValue := reflect.ValueOf(cb)
	wantsRaw := (argType == emptyMsgType)
	newCb := func(request *Passenger) {
		go func() {
			log.Debugf("Processing passenger for rocket %s", subject)
			var oV []reflect.Value
			if wantsRaw {
				oV = []reflect.Value{reflect.ValueOf(request)}
			} else {
				// Build function argument list
				var oPtr reflect.Value
				if argType.Kind() != reflect.Ptr {
					oPtr = reflect.New(argType)
				} else {
					oPtr = reflect.New(argType.Elem())
				}
				if request.Body != "" {
					if err := Decode([]byte(request.Body), oPtr.Interface()); err != nil {
						controlTower.ProcessError(request, &ErrorBody{InternalMessage: err.Error(), Message: DEFAULT_ERROR_MSG, Code: HTTP_INTERNAL_SERVER_ERROR,})
						return
					}
				}
				if argType.Kind() != reflect.Ptr {
					oPtr = reflect.Indirect(oPtr)
				}

				elem := reflect.ValueOf(oPtr.Interface()).Elem()
				if params != nil {
					parts := strings.Split(request.Subject, ".")
					for i := 0; i < len(params); i++ {
						param := params[i]
						field := elem.FieldByName(param.Name)
						if !field.IsValid() {
							log.WithFields(log.Fields{
								"passenger":  request,
								"paramIndex": param.Index,
								"paramName":  param.Name,
								"i":          i,
								"field":      field,
								"parts":      parts,
							}).Errorf("Unknown field when setting passenger params")
							controlTower.ProcessError(request, &ErrorBody{Message: "Unknown field " + param.Name, Code: HTTP_INTERNAL_SERVER_ERROR,})
							return
						}
						field.SetString(parts[param.Index])
					}
				}
				if (middlewares != nil) {
					for i := 0; i < len(middlewares); i++ {
						chainErr := middlewares[i](request, elem)
						if chainErr != nil {
							controlTower.ProcessError(request, chainErr)
						}
					}
				}

				switch numArgs {
				case 1:
					oV = []reflect.Value{oPtr}
				case 2:
					headersV := reflect.ValueOf(request.Headers)
					oV = []reflect.Value{headersV, oPtr}
				case 3:
					subV := reflect.ValueOf(request.Subject)
					headersV := reflect.ValueOf(request.Headers)
					oV = []reflect.Value{subV, headersV, oPtr}
				}
			}
			values := cbValue.Call(oV)
			if request.Reply == "" {
				// Nobody expecting a response
				return
			}
			response := Passenger{
				StatusCode: 200,
			}
			if len(values) > 0 {
				if values[len(values)-1].IsNil() == false {
					controlTower.ProcessError(request, values[len(values)-1].Interface().(*ErrorBody))
					return
				}

				if len(values) > 1 {
					jsonBody, _ := json.Marshal(values[0].Interface())
					response.Body = string(jsonBody)

				}
			}
			log.WithFields(log.Fields{
				"statusCode": response.StatusCode,
				"headers":    response.Headers,
				"body":       response.Body,
				"subject":    request.Reply,
			}).Debugf("Publishing response for rocket %s", subject)
			controlTower.NatsEncodedConn.Publish(request.Reply, response)
		}()
	}
	if queue == "" {
		controlTower.NatsEncodedConn.Subscribe(subject, newCb)
	} else {
		controlTower.NatsEncodedConn.QueueSubscribe(subject, queue, newCb)
	}

	return newCb, nil
}

func (c *ControlTower) Publish(request Passenger) (*ErrorBody) {
	err := c.NatsEncodedConn.Publish(request.Subject, request)
	if err != nil {
		return &ErrorBody{InternalMessage: err.Error(), Message: DEFAULT_ERROR_MSG, Code: HTTP_INTERNAL_SERVER_ERROR,}
	}
	return nil;
}

func (c *ControlTower) Launch(request Passenger) (Passenger, error) {
	inbox := nats.NewInbox()
	request.Reply = inbox
	sub, subErr := c.NatsConn.SubscribeSync(inbox)
	if subErr != nil {
		// TODO -  do something about it
		log.Errorf("Error creating subscriber %s", subErr.Error())
		return Passenger{}, subErr
	}
	sub.AutoUnsubscribe(1)
	log.WithFields(log.Fields{
		"subject":        request.Subject,
		"body":           request.Body,
		"replyToAddress": inbox,
	}).Debugf("Launching passenger")
	c.NatsEncodedConn.Publish(request.Subject, request)
	if request.TimeoutOverride == 0 {
		request.TimeoutOverride = c.NatsNextMsgTimeout;
	}

	m, nextMsgErr := sub.NextMsg(request.TimeoutOverride)
	if nextMsgErr != nil {
		// TODO -  do something about it
		log.Errorf("Error reading message %s", nextMsgErr.Error())
		return Passenger{}, nextMsgErr
	}
	bodyString := string(m.Data)
	log.WithFields(log.Fields{
		"subject":        request.Subject,
		"body":           bodyString,
		"replyToAddress": inbox,
	}).Debugf("Receive passenger back")
	var response Passenger
	json.Unmarshal([]byte(bodyString), &response)
	return response, nil
}

func (controlTower *ControlTower) ProcessError(request *Passenger, err *ErrorBody) {
	if request.Reply == "" {
		return
	}
	response := Passenger{}
	if err.Code == 0 {
		response.StatusCode = HTTP_INTERNAL_SERVER_ERROR
	} else {
		response.StatusCode = err.Code
	}
	err.Code = 0
	jsonBody, _ := json.Marshal(err)
	response.Body = string(jsonBody)
	log.WithFields(log.Fields{
		"statusCode": response.StatusCode,
		"headers":    response.Headers,
		"body":       response.Body,
		"subject":    request.Reply,
	}).Errorf("Processing error for %s", request.Subject)
	controlTower.NatsEncodedConn.Publish(request.Reply, response)
}

func Decode(data []byte, vPtr interface{}) (err error) {
	switch arg := vPtr.(type) {
	case *string:
		str := string(data)
		if strings.HasPrefix(str, `"`) && strings.HasSuffix(str, `"`) {
			*arg = str[1: len(str)-1]
		} else {
			*arg = str
		}
	case *[]byte:
		*arg = data
	default:
		err = json.Unmarshal(data, arg)
	}
	return
}

func argInfo(cb interface{}) (reflect.Type, int) {
	cbType := reflect.TypeOf(cb)
	if cbType.Kind() != reflect.Func {
		log.Fatalf("nats: Handler needs to be a func")
	}
	numArgs := cbType.NumIn()
	if numArgs == 0 {
		return nil, numArgs
	}
	return cbType.In(numArgs - 1), numArgs
}
