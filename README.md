Houston -- (inspired by Express.js)

The houston go client is a set of helpers around the native NATS client.

![Gopher](https://bitbucket.org/aytra/houston-go/issues/attachments/1/aytra/houston-go/1471266532.67/1/golang-gopher.png)
![Rocket](https://bitbucket.org/aytra/houston-go/issues/attachments/1/aytra/houston-go/1471266532.55/1/houston-rocket-icon.png)

## Motivation

While creating microservices at Aytra we saw that certain patterns of boiler-plating were starting to be duplicated across services worked by different developers. With houston we grouped some of those patterns and offered a set of pre-built constructs to make it easier to plug in NATS on new microserivces.
Houston still exposes the raw NATS API so it is possible to fallback and consume any features exposed by the NATS client.

## Features

* Middleware chainning
* Parameter mapping
* Error handling
* HTTP like data model

## Basic Usage
```go
controlTower := &houston.ControlTower{NatsUrl: "nats://localhost:4222"}
controlTower.Init()

type person struct {
     Id       string
     Name     string
     Address  string
     Age      int
}
rocket := houston.Rocket{Route: "service.v1.print", Params: nil, Group: "", Middleware: nil, Callback: func(p *person) (*houston.ErrorBody) {
  fmt.Printf("Hello %s\n", p.Name)
}}
controlTower.Build(rocket)
```

## Paramaters mapping
```go
controlTower := &houston.ControlTower{NatsUrl: "nats://localhost:4222"}
controlTower.Init()

type person struct {
     Id       string
     Name     string
     Address  string
     Age      int
}
rocket := houston.Rocket{Route: "service.v1.get.person.*", Params: houston.Params{{Index: 4, Name: "Id"},}, Group: "", Middleware: nil, Callback: func(p *person) (*houston.ErrorBody) {
  fmt.Printf("Trying to fetch person with id %d\n", p.Id)
}}
controlTower.Build(rocket)
```

## Middleware chainning
```go
controlTower := &houston.ControlTower{NatsUrl: "nats://localhost:4222"}
controlTower.Init()

type person struct {
     Id       string
     Name     string
     Address  string
     Age      int
}

func ValidateSessionMiddleware(request *Passenger, elem reflect.Value) (*ErrorBody){
  if request.Headers == nil || request.Headers["session"] == nil {
    return &ErrorBody{Message: DEFAULT_ERROR_MSG, Code: HTTP_BAD_REQUEST, InternalMessage: "Missing required session header attribute"}
  }
  return nil
}

func LoggerMiddleware(request *Passenger, elem reflect.Value) (*ErrorBody){
  fmt.Printf("request %v\n", request)
  return nil
}

rocket := houston.Rocket{Route: "service.v1.get.person.*", Params: houston.Params{{Index: 4, Name: "Id"},}, Group: "", Middleware: houston.Middlewares{Logger, ValidateSession}, Callback: func(p *person) (*houston.ErrorBody) {
  fmt.Printf("Trying to fetch person with id %d\n", p.Id)
}}
controlTower.Build(rocket)
```

## Launching passengers
```go
filter, _ := json.Marshal(person{Name: "Neil Armstrong"})
    request := houston.Passenger{
      Subject: "houston.rescue",
      Body: string(jsonFilter),
    }
    response, _ := controlTower.Launch(request)
    var p person
    json.Unmarshal([]byte(response.Body), &person)
    fmt.Printf("Rescued %s\n", p.Name)
```

## TODO

- [] Plug in to travis-ci 
- [] Support parameter interpolation in the rocket route
- [] Handle json parsing inside Launch function
- [] Add support to NATS cluster auto discover
- [] Publish library to gopkg
- [] Create docs
- [] Clean up logs
