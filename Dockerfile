FROM golang:1.8

RUN apt-get update

RUN cd /tmp && \
    curl -L https://github.com/Masterminds/glide/releases/download/v0.12.3/glide-v0.12.3-linux-amd64.tar.gz | tar xvz && \
    mv linux-amd64/glide /usr/local/bin/ && \
    chmod +x /usr/local/bin/glide

WORKDIR /go/src/bitbucket.org/aytra/houston-go